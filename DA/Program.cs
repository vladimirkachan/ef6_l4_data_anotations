﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DA.Data;
using DA.DataAccess;

namespace DA
{
    class Program
    {
        static void Main(string[] args)
        {
            using CodeContext db = new();
            Console.WriteLine(db.Database.Connection.ConnectionString);
            db.Attendees.Add(new Attendee { FirstName = "VovA", LastName = "KachaN", DateAdded = DateTime.Now });
            db.Attendees.Add(new Attendee { FirstName = "Masha", LastName = "Sokolova", DateAdded = DateTime.Now });
            db.SaveChanges();
            var attendees = db.Attendees.ToList();
            Console.WriteLine($"Count = {attendees.Count}");
            foreach(var a in attendees) Console.WriteLine(a);
            Console.ReadKey();
        }
    }
}
 