﻿using System;
using System.Data.Entity;
using System.Linq;
using DA.Data;

namespace DA.DataAccess
{
    public class CodeContext : DbContext
    {
        static CodeContext()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<CodeContext>());
        }
        public CodeContext()
            : base("name=CodeContext")
        {
        }
        public virtual DbSet<Attendee> Attendees { get; set; }
    }
}