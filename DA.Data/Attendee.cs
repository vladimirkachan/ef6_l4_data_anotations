﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA.Data
{
    //[Table("Att")]
    public class Attendee
    {
        [Key] 
        public int AttendeeKey {get; set;}

        [Required, MinLength(3), MaxLength(20)]
        public string FirstName {get; set;}

        [MaxLength(50)] 
        public string LastName {get; set;}

        public DateTime DateAdded {get; set;}

        public override string ToString()
        {
            return $"{AttendeeKey}. {FirstName} {LastName} <{DateAdded}>";
        }
    }
}
